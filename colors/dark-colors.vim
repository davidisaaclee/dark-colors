function! DLColorCommand(group, args)
	let styles = printf("highlight %s", a:group)

	for cmd in ["gui", "guifg", "guibg", "cterm"]
		if has_key(a:args, cmd)
			let styles = printf("%s %s=%s", styles, cmd, a:args[cmd])
		endif
	endfor

	return styles
endfunction

highlight clear

" Reset String -> Constant links etc if they were reset
if exists("syntax_on")
	syntax reset
endif

let colors_name = 'dark-colors'

" let s:light_green = '#4cb368'
" let s:light_blue = '#008ec2'
" let s:light_red = '#f36f70'
" let s:light_yellow = '#e9b417'
" let s:purple = '#9B30FF'
let s:light_green = '#5faf87'
let s:lighter_blue = '#d0e0ff'
" let s:light_blue = '#008080'
let s:light_blue = '#5f87af'
let s:light_red = '#d78787'
let s:light_yellow = '#afaf5f'
let s:purple = '#9B30FF'

let s:lighter_grey = '#b7b7b9'
let s:light_grey = '#808080'
let s:grey = '#575759'
let s:white = '#e0e3e6'
let s:alert = '#151515'
let s:important = '#000000'

let s:selection = '#7b309f'
let s:background = '#444444'
let s:foreground = '#c6c6c6'

let s:color_specs = {}

" ColorColumn	used for the columns set with 'colorcolumn'
" Conceal		placeholder characters substituted for concealed
" 		text (see 'conceallevel')
" Cursor		the character under the cursor
exec 'hi Cursor guifg=' . s:light_yellow ' guibg=' . s:important
" CursorIM	like Cursor, but used when in IME mode |CursorIM|
" CursorColumn	the screen column that the cursor is in when 'cursorcolumn' is
" 		set
hi default link CursorColumn CursorLine

" CursorLine	the screen line that the cursor is in when 'cursorline' is
" 		set
let s:color_specs['CursorLine'] = {
\	"cterm": 'NONE',
\	"guibg": '#2e2e2e' 
\}

" Directory	directory names (and other special names in listings)
let s:color_specs['Directory'] = { 'guifg': s:light_blue }
" DiffAdd		diff mode: Added line |diff.txt|
" DiffChange	diff mode: Changed line |diff.txt|
" DiffDelete	diff mode: Deleted line |diff.txt|
" DiffText	diff mode: Changed text within a changed line |diff.txt|
" EndOfBuffer	filler lines (~) after the last line in the buffer.
" 		By default, this is highlighted like |hl-NonText|.
" ErrorMsg	error messages on the command line
" VertSplit	the column separating vertically split windows
let s:color_specs['VertSplit'] = {
\ 'gui': 'NONE',
\ 'guibg': 'NONE',
\ 'guifg': 'NONE',
\ 'cterm': 'NONE',
\ 'ctermbg': 'NONE',
\ 'ctermfg': 'NONE'
\}
" Folded		line used for closed folds
" FoldColumn	'foldcolumn'
" SignColumn	column where |signs| are displayed
hi SignColumn guibg=NONE guifg=NONE ctermbg=NONE ctermfg=NONE
" IncSearch	'incsearch' highlighting; also used for the text replaced with
" 		":s///c"
" LineNr		Line number for ":number" and ":#" commands, and when 'number'
" 		or 'relativenumber' option is set.
exec 'hi LineNr guibg=NONE guifg=' . s:light_grey
" CursorLineNr	Like LineNr when 'cursorline' or 'relativenumber' is set for
" 		the cursor line.
exec 'hi CursorLineNr guibg=NONE guifg=' . s:light_green
" MatchParen	The character under the cursor or just before it, if it
" 		is a paired bracket, and its match. |pi_paren.txt|
exec 'hi MatchParen gui=NONE guifg=' . s:light_yellow . ' guibg=' . s:important
" ModeMsg		'showmode' message (e.g., "-- INSERT --")
exec 'hi ModeMsg guifg=' . s:light_yellow
" MoreMsg		|more-prompt|
" NonText		'@' at the end of the window, characters from 'showbreak'
" 		and other characters that do not really exist in the text
" 		(e.g., ">" displayed when a double-wide character doesn't
" 		fit at the end of the line).
exec 'hi NonText guifg=' . s:lighter_grey
" Normal		normal text
exec 'hi Normal guifg=' . s:foreground . ' guibg='s:background
" Pmenu		Popup menu: normal item.
exec 'hi Pmenu guibg=' . s:alert ' guifg=' . s:lighter_grey
" PmenuSel	Popup menu: selected item.
exec 'hi PmenuSel guibg=' . s:important ' guifg=' . s:light_blue
" PmenuSbar	Popup menu: scrollbar.
" PmenuThumb	Popup menu: Thumb of the scrollbar.
" Question	|hit-enter| prompt and yes/no questions
" QuickFixLine	Current |quickfix| item in the quickfix window.
" Search		Last search pattern highlighting (see 'hlsearch').
" 		Also used for similar items that need to stand out.
" SpecialKey	Meta and special keys listed with ":map", also for text used
" 		to show unprintable characters in the text, 'listchars'.
" 		Generally: text that is displayed differently from what it
" 		really is.
" SpellBad	Word that is not recognized by the spellchecker. |spell|
" 		This will be combined with the highlighting used otherwise.
" SpellCap	Word that should start with a capital. |spell|
" 		This will be combined with the highlighting used otherwise.
" SpellLocal	Word that is recognized by the spellchecker as one that is
" 		used in another region. |spell|
" 		This will be combined with the highlighting used otherwise.
" SpellRare	Word that is recognized by the spellchecker as one that is
" 		hardly ever used. |spell|
" 		This will be combined with the highlighting used otherwise.
" StatusLine	status line of current window
let s:color_specs['StatusLine'] = {
\ 'gui': 'NONE',
\ 'guibg': s:grey,
\ 'guifg': 'NONE',
\ 'cterm': 'NONE',
\ 'ctermfg': 'NONE'
\}
" StatusLineNC	status lines of not-current windows
" 		Note: if this is equal to "StatusLine" Vim will use "^^^" in
" 		the status line of the current window.
let s:color_specs['StatusLineNC'] = {
\ 'gui': 'NONE',
\ 'guibg': s:background,
\ 'guifg': 'NONE',
\ 'cterm': 'NONE',
\ 'ctermfg': 'NONE'
\}
" StatusLineTerm	status line of current window, if it is a |terminal| window.
" StatusLineTermNC   status lines of not-current windows that is a |terminal|
" 		window.
" TabLine		tab pages line, not active tab page label
exec 'hi TabLine gui=NONE guibg=' . s:grey . ' cterm=NONE'
" TabLineFill	tab pages line, where there are no labels
exec 'hi TabLineFill gui=NONE guibg=' . s:background . ' cterm=NONE ctermbg=NONE'
" TabLineSel	tab pages line, active tab page label
exec 'hi TabLineSel gui=bold guibg=' . s:light_blue ' guifg=' . s:white . ' cterm=bold'
" Terminal	|terminal| window (see |terminal-size-color|)
" Title		titles for output from ":set all", ":autocmd" etc.
exec 'hi Title gui=bold guibg=NONE guifg=' . s:light_red . ' cterm=bold'
" Visual		Visual mode selection
exec 'hi Visual guibg=' . s:selection
" VisualNOS	Visual mode selection when vim is "Not Owning the Selection".
" 		Only X11 Gui's |gui-x11| and |xterm-clipboard| supports this.
" WarningMsg	warning messages
" WildMenu	current match in 'wildmenu' completion

exec 'hi Comment gui=italic guifg=' . s:light_grey

exec 'hi Constant guifg=' . s:light_green

exec 'hi Identifier guifg=' . s:light_blue

exec 'hi Statement guifg=' . s:light_red
exec 'hi Keyword gui=underline guisp=' . s:lighter_grey

let s:color_specs['PreProc'] = {
\ 'gui': 'italic',
\ 'guifg' :s:light_red
\}

exec 'hi Type gui=bold cterm=bold guifg=' . s:light_green

exec 'hi Special gui=italic guifg=' . s:foreground

exec 'hi Error gui=underline guibg=NONE'
exec 'hi Todo gui=bold cterm=bold guibg=NONE guifg=' . s:purple

" JavaScript
hi link jsThis Identifier
hi link jsClassProperty jsClassFuncName
hi link jsExport Keyword

exec 'hi default jsFunction guifg=' . s:light_red
exec 'hi default jsFuncCall gui=underline guifg=' . s:foreground . ' guisp=' . s:lighter_grey
exec 'hi default jsFuncName guifg=' . s:light_blue
exec 'hi default jsStorageClass guifg=' . s:light_yellow
exec 'hi default jsClassFuncName guifg=' . s:light_blue
exec 'hi default jsClassDefinition gui=bold cterm=bold guifg=' . s:light_blue
exec 'hi default jsClassKeyword guifg=' . s:light_yellow
exec 'hi default jsExtendsKeyword guifg=' . s:light_yellow
exec 'hi jsExport gui=underline guifg=' . s:light_red
exec 'hi default jsExportDefault gui=underline guifg=' . s:light_red

" Flow
hi link jsFlowClassDef Type
hi link jsFlowType Type
hi link jsFlowClassGroup jsFlowType
hi link jsFlowTypeStatement Type

" TypeScript
exec 'hi typescriptEndColons guifg=NONE'
exec 'hi typescriptExceptions guifg=' . s:light_yellow
exec 'hi typescriptParens guifg=' . s:light_blue
exec 'hi typescriptFuncKeyword guifg=' . s:light_yellow
exec 'hi typescriptReserved guifg=' . s:light_yellow

" XML (JSX)
exec 'hi xmlTag guifg=' . s:light_blue
hi link xmlEndTag xmlTag
exec 'hi xmlTagName guifg=' . s:light_blue
exec 'hi xmlAttrib guifg=' . s:light_grey
hi link xmlEqual xmlAttrib

" ALE status
exec printf("hi SpellBad gui=NONE cterm=NONE ctermbg=NONE guibg=NONE")
exec printf("hi ALEErrorLine gui=NONE cterm=NONE guibg=#3c2121")

" gitgutter
exec printf("highlight GitGutterAdd guifg=%s", s:light_green)
exec printf("highlight GitGutterChange guifg=%s", s:light_yellow)
exec printf("highlight GitGutterDelete guifg=%s", s:light_red)

" Apply all specs
for spec in items(s:color_specs)
	exec DLColorCommand(spec[0], spec[1])
endfor

" map for quickly iterating
" map <Leader>\ :w<CR>:so %<CR>
